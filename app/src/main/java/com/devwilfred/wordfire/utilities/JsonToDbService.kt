package com.devwilfred.wordfire.utilities

import android.app.IntentService
import android.content.Intent
import com.devwilfred.wordfire.R
import com.devwilfred.wordfire.database.Verse
import com.devwilfred.wordfire.database.BibleVerse
import com.google.gson.GsonBuilder
import com.google.gson.stream.JsonReader
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.kotlin.createObject
import java.io.InputStreamReader

class JsonToDbService : IntentService("fetchBible") {

    override fun onHandleIntent(p0: Intent?) {

        Realm.init(applicationContext)
        val c = RealmConfiguration.Builder()
                .name(p0!!.getStringExtra("version"))
                .deleteRealmIfMigrationNeeded()
                .schemaVersion(1).build()

        Realm.setDefaultConfiguration(c)

        val realm = Realm.getDefaultInstance()

        val gson = GsonBuilder().create()

        val reader = JsonReader(InputStreamReader(resources.openRawResource(R.raw.bbe), "UTF-8"))
        reader.beginArray()

        realm.executeTransaction {

            rDb ->
            while (reader.hasNext()) {
                val verse: Verse = gson.fromJson(reader, Verse::class.java)
                val bibleVerse: BibleVerse = rDb.createObject()
                bibleVerse.book = verse.field[1]
                bibleVerse.chapter = verse.field[2]
                bibleVerse.v = verse.field[3]
                bibleVerse.word = verse.field[4]
                //Log.e("kjhgfkkjhgf", bibleVerse.word)
                rDb.insert(bibleVerse)
            }

        }

        //Realm.compactRealm(realm.configuration)
        val compatDb = CompressRealmSize()
        compatDb.compactD("bbe")
        realm.close()
    }

}