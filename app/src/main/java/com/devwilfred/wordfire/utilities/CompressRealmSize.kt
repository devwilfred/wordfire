package com.devwilfred.wordfire.utilities

import io.realm.Realm
import io.realm.RealmConfiguration
import java.io.File

class CompressRealmSize {

    fun compactD(dbName: String) {
        val db = Realm.getInstance(getConfig(dbName))
        val compactedFile = File(db.configuration.realmDirectory, "$dbName-compacted")
        compactedFile.delete()
        db.writeCopyTo(compactedFile)
        db.close()

        val compactedDb = Realm.getInstance(getConfig("$dbName-compacted"))
        val dbFile = File(compactedDb.configuration.realmDirectory, dbName)
        dbFile.delete()
        compactedDb.writeCopyTo(dbFile)
        compactedDb.close()
        compactedFile.delete()

    }

    private fun getConfig(name: String): RealmConfiguration {
        return RealmConfiguration.Builder()
                .name(name)
                .deleteRealmIfMigrationNeeded()
                .schemaVersion(1)
                .build()
    }
}