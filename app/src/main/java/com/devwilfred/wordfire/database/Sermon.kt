package com.devwilfred.wordfire.database

import io.realm.RealmObject
import io.realm.annotations.Ignore
import io.realm.annotations.PrimaryKey
import java.util.*
import java.util.concurrent.atomic.AtomicInteger
import java.util.UUID.randomUUID




open class Sermon : RealmObject() {

    @PrimaryKey
    var id : Long = UUID.randomUUID().mostSignificantBits

    var date : Date = Date()
    var sermonTxt : String = ""
    var topic : String = ""
    var isPinned : Boolean = false
}