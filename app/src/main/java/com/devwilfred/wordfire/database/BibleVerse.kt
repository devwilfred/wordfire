package com.devwilfred.wordfire.database

import io.realm.RealmObject

open class BibleVerse : RealmObject() {
    var book : String = ""
    var chapter : String = ""
    var v : String = ""
    var word : String = ""
    
    
}