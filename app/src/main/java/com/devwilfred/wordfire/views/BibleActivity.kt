package com.devwilfred.wordfire.views

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import com.devwilfred.wordfire.R
import com.devwilfred.wordfire.adapterUtil.RealmAdapter
import com.devwilfred.wordfire.database.BibleVerse
import com.devwilfred.wordfire.utilities.CompressRealmSize
import java.util.*
import io.realm.*
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.bible_fragment.*
import java.text.SimpleDateFormat
import kotlin.collections.ArrayList


class BibleActivity : AppCompatActivity() {
// TODO fix the bugs in the next and prev buttons
// TODO persist the last opened book

    private val bundle = Bundle()
    var book: String? = ""
    var chapter = 1
    var bookIndex = 1
    lateinit var scanner: Scanner
    private val bibleBooks = ArrayList<String>()
    private lateinit var realm: Realm
    lateinit var realmAdapter: RealmAdapter



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bible_fragment)



        scanner = Scanner(resources.openRawResource(R.raw.booksofthebible))
        while (scanner.hasNext())
            bibleBooks.add(scanner.nextLine())

        if (intent != null && intent.getStringExtra("scrip") != null) {
            val scrip = intent.getStringExtra("scrip").split(",")
            populateRecy(scrip[0], scrip[1])
        } else
            populateRecy("41", "1")

        bar.replaceMenu(R.menu.bottom_menu)
        bar.setNavigationOnClickListener { _ ->
            val bottomSheetDialogFragment = BottomSheet()
            val purposeBundle = Bundle()
            purposeBundle.putString("purpose", "navigation")
            bottomSheetDialogFragment.arguments = purposeBundle
            bottomSheetDialogFragment.show(supportFragmentManager, bottomSheetDialogFragment.tag)
        }

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        next.setOnClickListener { _ ->
            bundle.putString("script", "$bookIndex,${chapter + 1}")
            populateRecy("$bookIndex", "${chapter + 1}")
        }

        prev.setOnClickListener { _ ->
            bundle.putString("script", "$bookIndex,${chapter - 1}")
            populateRecy("$bookIndex", "${chapter - 1}")
        }

        open_button.setOnClickListener { _ ->
            val bottomSheetDialogFragment = BottomSheet()
            val purposeBundle = Bundle()
            purposeBundle.putString("purpose", "open scripture")
            bottomSheetDialogFragment.arguments = purposeBundle
            bottomSheetDialogFragment.show(supportFragmentManager, bottomSheetDialogFragment.tag)
        }


        loading_bible.visibility = View.GONE
    }

    fun populateRecy(book: String, pchapter : String) {
        bookIndex = book.toInt()
        chapter = pchapter.toInt()

        val t = "${bibleBooks[bookIndex - 1]} : $chapter"
        open_button.text = t
        realm = Realm.getDefaultInstance()
        val res = realm.where<BibleVerse>()
                .equalTo("book", book)
                .and().equalTo("chapter", pchapter)
                .findAllAsync()
        realmAdapter = RealmAdapter(res)
        recy.layoutManager = LinearLayoutManager(this)
        recy.adapter = realmAdapter
        recy.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
    }


    override fun onDestroy() {
        val compatDb = CompressRealmSize()
        compatDb.compactD("bbe")
        if (!realm.isClosed)
            realm.close()
        super.onDestroy()

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
            else -> {

            }
        }
        return true
    }

}
