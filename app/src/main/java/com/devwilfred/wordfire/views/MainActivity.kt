package com.devwilfred.wordfire.views

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.devwilfred.wordfire.database.BibleVerse
import com.devwilfred.wordfire.utilities.CompressRealmSize
import io.realm.Realm
import io.realm.RealmConfiguration

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val compatDb = CompressRealmSize()

        val c = RealmConfiguration.Builder()
                .name("bbe")
                .deleteRealmIfMigrationNeeded()
                .schemaVersion(1).build()

        Realm.setDefaultConfiguration(c)

        val realm = Realm.getDefaultInstance()
        val r = realm.where(BibleVerse::class.java).findFirst()
        compatDb.compactD("bbe")
        realm.close()
        if (r == null) {
            startActivity(Intent(this, IntroActivity::class.java))
            finish()
        } else {
            startActivity(Intent(this, SermonListActivity::class.java))
            finish()
        }
    }
}