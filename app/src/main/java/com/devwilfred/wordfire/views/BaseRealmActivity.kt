package com.devwilfred.wordfire.views

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.devwilfred.wordfire.utilities.CompressRealmSize
import io.realm.Realm
import io.realm.RealmConfiguration

open class BaseRealmActivity : AppCompatActivity() {
    private lateinit var realm: Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val config = RealmConfiguration.Builder()
                .name("sermons")
                .deleteRealmIfMigrationNeeded()
                .schemaVersion(1)
                .build()
        realm = Realm.getInstance(config)
    }


    override fun onDestroy() {
        val compatDb = CompressRealmSize()
        compatDb.compactD("sermons")
        if (!realm.isClosed)
            realm.close()

        super.onDestroy()
    }

    fun getRealm(): Realm {
        return realm
    }
}