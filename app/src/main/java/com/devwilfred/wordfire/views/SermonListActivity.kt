package com.devwilfred.wordfire.views

import android.content.Intent
import android.os.Bundle
import android.provider.Browser
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.text.TextUtils
import com.devwilfred.wordfire.R
import com.devwilfred.wordfire.adapterUtil.PinnedAdapter
import com.devwilfred.wordfire.adapterUtil.SermonAdapter
import com.devwilfred.wordfire.database.Sermon
import io.realm.kotlin.where

import kotlinx.android.synthetic.main.activity_home.*

class SermonListActivity : BaseRealmActivity(), SermonAdapter.ItemClickListener, PinnedAdapter.ItemClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        bar.replaceMenu(R.menu.bottom_menu)
       bar.setNavigationOnClickListener { _ ->
            val bottomSheetDialogFragment = BottomSheet()
            val purposeBundle = Bundle()
            purposeBundle.putString("purpose", "navigation")
            bottomSheetDialogFragment.arguments = purposeBundle
            bottomSheetDialogFragment.show(supportFragmentManager, bottomSheetDialogFragment.tag)
        }
        setSupportActionBar(home_toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.title = getString(R.string.app_name)

        fab.setOnClickListener { _ ->
            startActivity(Intent(this, BibleActivity::class.java))
        }

        add_sermon.setOnClickListener { _ ->
            startActivity(Intent(this, EditorActivity::class.java))
        }

        setUpRecycler()
    }

    override fun onItemClickListener(itemId: Long) {
        val updateIntent = Intent(this, EditorActivity::class.java)
        updateIntent.putExtra("update", itemId)
        updateIntent.putExtra("updateCheck", 1)
        startActivity(updateIntent)
    }

    private fun setUpRecycler() {
        val res = getRealm().where<Sermon>()
                .equalTo("isPinned", false)
                .findAllAsync()

        sermon_recycler.adapter = SermonAdapter(res, this)
        sermon_recycler.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        val mLinear = LinearLayoutManager(this)
        mLinear.reverseLayout = true
        mLinear.stackFromEnd = true
        sermon_recycler.layoutManager = mLinear
        sermon_recycler.setHasFixedSize(true)

        ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
                return false
            }

            // Called when a user swipes left or right on a ViewHolder
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
                // Here is where you'll implement swipe to delete
                when (swipeDir) {
                    ItemTouchHelper.LEFT -> {
                        getRealm().executeTransaction {
                            res[viewHolder.adapterPosition]!!.deleteFromRealm()
                        }
                    }
                    else -> {
                        getRealm().executeTransaction {
                            realm -> val toPin = res[viewHolder.adapterPosition]!!
                            toPin.isPinned = true
                            realm.insertOrUpdate(toPin)
                        }
                    }
                }


            }
        }).attachToRecyclerView(sermon_recycler)


       /* val pinned = getRealm().where<Sermon>().equalTo("isPinned", true)
                .findAllAsync()*/
        // pinned_recycler.adapter = PinnedAdapter(pinned, this)
        // pinned_recycler.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        // pinned_recycler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true)
    }


    override fun startActivity(intent: Intent) {
        var handled = false

        if (TextUtils.equals(intent.action, Intent.ACTION_VIEW)) {

            val app_id = intent.getStringExtra(Browser.EXTRA_APPLICATION_ID)
            if (TextUtils.equals(packageName, app_id)) {
                val scrip = intent.dataString.replace(".", ",")

                val bibleIntent = Intent(this, BibleActivity::class.java)
                bibleIntent.putExtra("scrip", scrip)

                handled = true
            }
        }

        if (!handled)
            super.startActivity(intent)
    }


}

