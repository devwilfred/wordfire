package com.devwilfred.wordfire.views

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.devwilfred.wordfire.R
import com.devwilfred.wordfire.utilities.JsonToDbService
import kotlinx.android.synthetic.main.intro_fragment.*

class IntroActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
setContentView(R.layout.intro_fragment)

        Log.e("fdsfdghjk", "on create")


        bbe_btn.setOnClickListener {
            _ -> loading_bible.visibility = View.VISIBLE
            val loadBibleIntent = Intent(this, JsonToDbService::class.java)
            loadBibleIntent.putExtra("version", "bbe")
            startService(loadBibleIntent)
        }

        //v.findViewById<LinearLayout>(R.id.make_invisible).visibility = View.INVISIBLE
        next_btn.visibility = View.VISIBLE
        next_btn.setOnClickListener {
            _ -> startActivity(Intent(this, SermonListActivity::class.java))
            finish()
        }
    }
}