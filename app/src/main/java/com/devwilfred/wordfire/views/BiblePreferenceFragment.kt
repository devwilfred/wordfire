package com.devwilfred.wordfire.views

import android.os.Bundle
import android.support.v7.preference.PreferenceFragmentCompat
import com.devwilfred.wordfire.R

class BiblePreferenceFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(p0: Bundle?, p1: String?) {
        addPreferencesFromResource(R.xml.app_preferences)
    }
}