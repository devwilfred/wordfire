package com.devwilfred.wordfire.views

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.Browser
import android.text.Editable
import android.text.InputType
import android.text.TextUtils
import android.text.TextWatcher
import android.text.method.LinkMovementMethod
import android.text.style.URLSpan
import android.text.util.Linkify
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import com.devwilfred.wordfire.R
import com.devwilfred.wordfire.database.Sermon
import com.devwilfred.wordfire.utilities.CompressRealmSize
import io.reactivex.Observable
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.kotlin.createObject
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.activity_editor.*
import kotlinx.android.synthetic.main.intro_fragment.view.*
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern

class EditorActivity : BaseRealmActivity() {

    var mode = false
    var updateSermon : Sermon? = null
    lateinit var upSermon : Sermon

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editor)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        if (intent != null && intent.getIntExtra("updateCheck", 0) != 0) {
            mode = true
            updateSermon = getRealm().where<Sermon>()
                    .equalTo("id", intent.getLongExtra("update", 0))
                    .findFirst()

            upSermon = Sermon()
            upSermon.id = intent.getLongExtra("update", 0)

            topicTv.setText(updateSermon?.topic)
            expo.setText(updateSermon?.sermonTxt)
            supportActionBar?.title = updateSermon?.topic
        }

        topicTv.movementMethod = LinkMovementMethod.getInstance()
        topicTv.addTextChangedListener(object : TextWatcher {
            var pattern = Pattern.compile("[0-9]*[A-Z][a-z]+.[0-9]+.[0-9]+")
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                Observable.just(s).debounce(1000, TimeUnit.MILLISECONDS)
                        .switchMap<String> {
                            it ->
                            return@switchMap Observable.just(it.toString())
                        }.subscribe {
                            str ->
                            supportActionBar?.title = str
                        }
            }

            override fun afterTextChanged(s: Editable) {
                Linkify.addLinks(s, pattern, "")
            }
        })

        expo.movementMethod = LinkMovementMethod.getInstance()
        expo.addTextChangedListener(object : TextWatcher {
            var pattern = Pattern.compile("[0-9]*[A-Z][a-z]+.[0-9]+.[0-9]+")
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                Linkify.addLinks(s, pattern, "")
                // s.getSpans(0, s.length, URLSpan::class.java)
            }
        })

    }

    override fun onBackPressed() {
        getRealm().executeTransaction { db ->

            if (mode) {
                upSermon.topic = topicTv.text.toString()
                upSermon.sermonTxt = expo.text.toString()
                db.insertOrUpdate(upSermon)
            } else {
                if (topicTv.text.toString() == "" && expo.text.toString() == "")
                    Toast.makeText(this, "Empty, Not saved", Toast.LENGTH_SHORT).show()
                else{


                    val sermon = Sermon()

                    sermon.date = Date()
                    sermon.id = UUID.randomUUID().mostSignificantBits
                    sermon.sermonTxt = expo.text.toString()
                    sermon.topic = topicTv.text.toString()
                    db.insertOrUpdate(sermon)
                }
            }
        }
        super.onBackPressed()
    }


    override fun startActivity(intent: Intent) {
        var handled = false


        if (TextUtils.equals(intent.action, Intent.ACTION_VIEW)) {

            Log.e("dwfesgfhbj,","link")
            val app_id = intent.getStringExtra(Browser.EXTRA_APPLICATION_ID)
            if (TextUtils.equals(packageName, app_id)) {
                val scrip = intent.dataString.replace(".", ",")

                val bibleIntent = Intent(this, BibleActivity::class.java)
                bibleIntent.putExtra("scrip", scrip)

                handled = true
            }
        }

        if (!handled)
            super.startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
            else -> {
            }
        }
        return true
    }

}
