package com.devwilfred.wordfire.views

import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.BottomSheetDialogFragment
import android.view.View
import android.widget.*
import com.devwilfred.wordfire.R
import java.util.*


class BottomSheet : BottomSheetDialogFragment() {
    private lateinit var purpose : String
    private lateinit var contentView : View
    private val bibleBooks = ArrayList<String>()
    private val loaderid: Int = 232

    override fun setArguments(args: Bundle?) {
        super.setArguments(args)
        purpose = args?.getString("purpose")!!

    }
    private val mBottomSheetBehaviorCallback = object : BottomSheetBehavior.BottomSheetCallback() {

       override fun onStateChanged(bottomSheet: View, newState: Int) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss()
            }

        }

        override fun onSlide(bottomSheet: View, slideOffset: Float) {}
    }

    override fun setupDialog(dialog: Dialog, style: Int) {

        when (purpose) {
            "navigation" -> {
                contentView = View.inflate(context, R.layout.nav_view, null)

                dialog.setContentView(contentView)
            }
            "open scripture" -> {
                contentView = View.inflate(context, R.layout.open_scripture, null)

                val scanner = Scanner(resources.openRawResource(R.raw.booksofthebible))
                while (scanner.hasNext())
                    bibleBooks.add(scanner.nextLine())

                dialog.setContentView(contentView)
                val arrayAdapter = ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, bibleBooks)
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                val spinner = dialog.findViewById<Spinner>(R.id.book_spinner)
                spinner.adapter = arrayAdapter

                val button = dialog.findViewById<Button>(R.id.go_scripture)
                val chapterEt = dialog.findViewById<EditText>(R.id.et_chapter)
                button.setOnClickListener {
                    _ ->
                    (activity as BibleActivity).populateRecy("${spinner.selectedItemId + 1}", chapterEt.text.toString())
                    dismiss()
                }
            }
            else -> {
            }
        }




        val params = (contentView.getParent() as View).getLayoutParams() as CoordinatorLayout.LayoutParams
        val behavior = params.behavior

        if (behavior != null && behavior is BottomSheetBehavior<*>) {
            behavior.setBottomSheetCallback(mBottomSheetBehaviorCallback)
        }

        /*dialog.setOnShowListener {
            _ -> val bottomSheetDialog = dialog as BottomSheetDialog
            val frameLayout : FrameLayout? =
                    bottomSheetDialog.findViewById(android.support.design.R.id.design_bottom_sheet)
            BottomSheetBehavior.from(frameLayout).state = BottomSheetBehavior.STATE_EXPANDED
        }*/
    }
}