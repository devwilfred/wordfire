package com.devwilfred.wordfire.adapterUtil

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.devwilfred.wordfire.R
import com.devwilfred.wordfire.database.Sermon
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

class PinnedAdapter(ordered : OrderedRealmCollection<Sermon>, val itemClickListener: ItemClickListener) :
        RealmRecyclerViewAdapter<Sermon, PinnedAdapter.Holder>(ordered, true) {


    interface ItemClickListener {
        fun onItemClickListener(itemId: Long)
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): Holder {
        return Holder(LayoutInflater.from(p0.context).inflate(R.layout.sermon_item, p0, false))
    }

    override fun onBindViewHolder(p0: Holder, p1: Int) {
        p0.bind(getItem(p1)!!)
    }

    inner class Holder(v : View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var pinnedTitle: TextView = v.findViewById(R.id.topic)
        private var dateTv: TextView = v.findViewById(R.id.date)
        private val pattern : Pattern
        private val dateForm = "dd/MM/yyy"
        private val dateFormat : SimpleDateFormat
        lateinit var mSermon: Sermon

        init {
            dateTv = v.findViewById(R.id.date)
            pattern = Pattern.compile("[A-z]+.+[0-9]+.+[0-9]")
            dateFormat = SimpleDateFormat(dateForm, Locale.getDefault())
            v.setOnClickListener(this)
        }

        fun bind(sermon : Sermon) {
            mSermon = sermon

            pinnedTitle.text = sermon.topic
            pinnedTitle.visibility = View.VISIBLE

            dateTv.text = dateFormat.format(sermon.date)

        }


        override fun onClick(item: View) {
            itemClickListener.onItemClickListener(getItem(adapterPosition)!!.id)
        }
    }
}
