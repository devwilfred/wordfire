package com.devwilfred.wordfire.adapterUtil

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.devwilfred.wordfire.R
import com.devwilfred.wordfire.database.BibleVerse
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter

class RealmAdapter(ordered : OrderedRealmCollection<BibleVerse>) :
        RealmRecyclerViewAdapter<BibleVerse, RealmAdapter.Holder>(ordered, true) {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): Holder {
        return Holder(LayoutInflater.from(p0.context).inflate(R.layout.item, p0, false))
    }

    override fun onBindViewHolder(p0: Holder, p1: Int) {
        p0.bind(getItem(p1)!!)
    }

    class Holder(v : View) : RecyclerView.ViewHolder(v) {

        var textView : TextView = v.findViewById(R.id.verse)
        fun bind(txt : BibleVerse) {
            val t = "${txt.v} -> ${txt.word}"
            textView.text = t
        }


    }
}
