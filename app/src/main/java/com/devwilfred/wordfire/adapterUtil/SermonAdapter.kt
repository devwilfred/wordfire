package com.devwilfred.wordfire.adapterUtil

import android.support.v7.widget.RecyclerView
import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.devwilfred.wordfire.R
import com.devwilfred.wordfire.database.Sermon
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

class SermonAdapter(ordered : OrderedRealmCollection<Sermon>, val itemClickListener: ItemClickListener) :
        RealmRecyclerViewAdapter<Sermon, SermonAdapter.Holder>(ordered, true) {

    init {
        setHasStableIds(true)
    }
    interface ItemClickListener {
        fun onItemClickListener(itemId: Long)
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): Holder {

        return Holder(LayoutInflater.from(p0.context).inflate(R.layout.sermon_item, p0, false))
    }

    override fun onBindViewHolder(p0: Holder, p1: Int) {
        p0.data = getItem(p1)
        p0.bind(getItem(p1)!!)
    }

    override fun getItemId(position: Int): Long {
        return getItem(position)!!.id
    }

    inner class Holder(val v : View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var topicTv: TextView = v.findViewById(R.id.topic)
        private var messageTv: TextView = v.findViewById(R.id.message)
        private var dateTv: TextView = v.findViewById(R.id.date)
        private val pattern : Pattern
        private val dateForm = "dd/MM/yyy"
        private val dateFormat : SimpleDateFormat
        lateinit var mSermon: Sermon
        var data : Sermon? = null

        init {
            dateTv = v.findViewById(R.id.date)
            pattern = Pattern.compile("[A-z]+.+[0-9]+.+[0-9]")
            dateFormat = SimpleDateFormat(dateForm, Locale.getDefault())
            v.setOnClickListener(this)
        }

        fun bind(sermon : Sermon) {
            mSermon = sermon

            if (sermon.topic != "") {
                topicTv.text = sermon.topic
                topicTv.visibility = View.VISIBLE
            } else {
                topicTv.visibility = View.GONE
            }
            if (sermon.sermonTxt != "") {
                messageTv.text = sermon.sermonTxt
                messageTv.visibility = View.VISIBLE
            } else {
                messageTv.visibility = View.GONE
            }
            dateTv.text = dateFormat.format(sermon.date)

        }


        override fun onClick(item: View) {
            itemClickListener.onItemClickListener(getItem(adapterPosition)!!.id)
        }
    }
}
